package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.Random;

public class CreditCard implements Serializable, Cloneable {
    String creditCardNumber;
    String creditCardName;
    BigDecimal balance;

    public CreditCard(String creditCardName, BigDecimal balance) {
        this.creditCardName = creditCardName;
        this.balance = balance;
        Random rand = new Random();
        this.creditCardNumber = String.format((Locale) null, //don't want any thousand separators
                "%04d %04d %04d %04d",
                rand.nextInt(10000),
                rand.nextInt(10000),
                rand.nextInt(10000),
                rand.nextInt(10000));
    }

    public void setPocketName(String creditCardName) {
        this.creditCardName = creditCardName;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public String getCreditCardName() {
        return creditCardName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public CreditCard clone() {
        try {
            CreditCard clone = (CreditCard) super.clone();
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
