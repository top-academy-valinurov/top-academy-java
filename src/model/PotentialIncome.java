package model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

public class PotentialIncome implements Serializable, Cloneable {
    BigDecimal amount;
    String pocketNumber;
    String cardNumber;
    String comment;
    String id;

    public PotentialIncome(BigDecimal amount, String pocketNumber, String cardNumber, String comment) {
        this.amount = amount;
        this.pocketNumber = pocketNumber;
        this.cardNumber = cardNumber;
        this.comment = comment;
        this.id = UUID.randomUUID().toString();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPocketNumber() {
        return pocketNumber;
    }

    public void setPocketNumber(String pocketNumber) {
        this.pocketNumber = pocketNumber;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    @Override
    public PotentialIncome clone() {
        try {
            PotentialIncome clone = (PotentialIncome) super.clone();
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
