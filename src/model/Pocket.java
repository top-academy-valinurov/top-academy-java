package model;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Locale;
import java.util.Random;

public class Pocket implements Serializable, Cloneable {
    String pocketNumber;
    String pocketName;
    BigDecimal balance;

    public Pocket( String pocketName, BigDecimal balance) {
        this.pocketName = pocketName;
        this.balance = balance;
        Random rand = new Random();
        this.pocketNumber = String.format((Locale)null, //don't want any thousand separators
                "%04d %04d %04d %04d",
                rand.nextInt(10000),
                rand.nextInt(10000),
                rand.nextInt(10000),
                rand.nextInt(10000));
    }

    public void setPocketName(String pocketName) {
        this.pocketName = pocketName;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getPocketNumber() {
        return pocketNumber;
    }

    public String getPocketName() {
        return pocketName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public Pocket clone() {
        try {
            Pocket clone = (Pocket) super.clone();
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }
}
