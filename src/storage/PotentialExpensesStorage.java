package storage;

import model.CurrentExpenses;
import model.Pocket;
import model.PotentialExpenses;

import java.util.ArrayList;
import java.util.List;

public interface PotentialExpensesStorage {

    void initPotentialExpenses();
    ArrayList<PotentialExpenses> getPotentialExpensesList();

    PotentialExpenses getPotentialExpenses(String id);
    PotentialExpenses getPotentialExpensesByPositionInList(int potentialExpensesPosition);
    void addPotentialExpenses(PotentialExpenses potentialExpenses);

    void updatePotentialExpenses(PotentialExpenses oldPotentialExpenses, PotentialExpenses newPotentialExpenses);

    void deletePotentialExpenses(int position);
}
