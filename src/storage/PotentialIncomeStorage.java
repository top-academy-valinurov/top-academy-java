package storage;

import model.PotentialIncome;

import java.util.ArrayList;

public interface PotentialIncomeStorage {

    void initPotentialIncome();

    ArrayList<PotentialIncome> getPotentialIncomeList();

    PotentialIncome getPotentialIncome(String id);

    PotentialIncome getPotentialIncomeByPositionInList(int potentialIncomePosition);

    void addPotentialIncome(PotentialIncome potentialIncome);

    void updatePotentialIncome(PotentialIncome oldPotentialIncome, PotentialIncome newPotentialIncome);

    void deletePotentialIncome(int position);
}
