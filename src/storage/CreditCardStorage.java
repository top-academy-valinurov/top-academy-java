package storage;

import model.CreditCard;

import java.util.ArrayList;

public interface CreditCardStorage {

    void initCreditCards();
    ArrayList<CreditCard> getCreditCardList();

    CreditCard getCreditCard(String creditCardNumber);
    CreditCard getCreditCardByPositionInList(int creditCardPosition);
    void addCreditCard(CreditCard creditCard);

    void updateCreditCard(CreditCard oldCreditCard, CreditCard newCreditCard);

    void deleteCreditCard(int position);
}
