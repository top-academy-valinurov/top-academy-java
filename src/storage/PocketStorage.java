package storage;

import model.Pocket;

import java.util.ArrayList;

public interface PocketStorage  {

    void initPockets();
    ArrayList<Pocket> getPocketList();

    Pocket getPocket(String pocketNumber);
    Pocket getPocketByPositionInList(int pocketPosition);
    void addPocket(Pocket pocket);

    void updatePocket(Pocket oldPocket, Pocket newPocket);

    void deletePocket(int position);
}
