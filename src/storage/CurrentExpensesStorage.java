package storage;

import model.CurrentExpenses;

import java.util.ArrayList;

public interface CurrentExpensesStorage {

    void initCurrentExpenses();
    ArrayList<CurrentExpenses> getCurrentExpensesList();

    CurrentExpenses getCurrentExpenses(String id);
    CurrentExpenses getCurrentExpensesByPositionInList(int currentExpensesPosition);
    void addCurrentExpenses(CurrentExpenses currentExpenses);

    void updateCurrentExpenses(CurrentExpenses oldCurrentExpenses, CurrentExpenses newCurrentExpenses);

    void deleteCurrentExpenses(int position);
}
