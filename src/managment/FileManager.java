package managment;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


public class FileManager {

    public FileManager() {
    }

    public <T> void serialize(List<T> list, String fullName) {
        try {
            FileOutputStream fileOut = new FileOutputStream(fullName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            for (T element : list) {
                if (!(element instanceof EofIndicatorClass)) {
                    out.writeObject(element);
                }
            }
            out.writeObject(new EofIndicatorClass());
            out.close();
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public <T> ArrayList<T> deSerialize(String fullName) {
        ArrayList<T> list = new ArrayList<>();
        File file = new File(fullName);
        if (file.length() > 0) {
            try {
                FileInputStream fileIn = new FileInputStream(fullName);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                Object obj;
                while (!((obj = in.readObject()) instanceof EofIndicatorClass)) {
                    T element = (T) obj;
                    list.add(element);
                }
                in.close();
            } catch (EOFException e) {
                System.out.println("EOFException");
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                System.out.println("FileNotFoundException");
                e.printStackTrace();
            } catch (IOException e) {
                System.out.println("IOException");
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                System.out.println("ClassNotFoundException");
                e.printStackTrace();
            }
        }

        return list;
    }

    public void createFiles(String fullName, String dirName) {
        File file = new File(fullName);
        File dirFile = new File(dirName);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void deleteDirectory(String dirName) {
        Path path = Paths.get(dirName);
        try {
            Files.walk(path)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}

class EofIndicatorClass implements Serializable {
}
