package managment;

import model.CurrentExpenses;
import storage.CurrentExpensesStorage;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class CurrentExpensesStorageImpl implements CurrentExpensesStorage, Serializable {

    private final FileManager fileManager;

    public CurrentExpensesStorageImpl(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    static private final String fileName = "CurrentExpenses.txt";
    static private final String dirName = "database";
    static private final String fullName = dirName + File.separator + fileName;

    @Override
    public void initCurrentExpenses() {
        fileManager.createFiles(fullName, dirName);
    }

    @Override
    public ArrayList<CurrentExpenses> getCurrentExpensesList() {
        return fileManager.deSerialize(fullName);
    }

    @Override
    public CurrentExpenses getCurrentExpenses(String id) {
        List<CurrentExpenses> list = getCurrentExpensesList();
        return list.stream().filter(currentExpenses -> currentExpenses.getId().equals(id)).toList().getFirst();
    }

    @Override
    public CurrentExpenses getCurrentExpensesByPositionInList(int currentExpensesPosition) {
        return getCurrentExpensesList().get(currentExpensesPosition);
    }

    @Override
    public void addCurrentExpenses(CurrentExpenses currentExpenses) {
        ArrayList<CurrentExpenses> list = fileManager.deSerialize(fullName);
        list.add(currentExpenses);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void updateCurrentExpenses(CurrentExpenses oldCurrentExpenses, CurrentExpenses newCurrentExpenses) {
        List<CurrentExpenses> list = getCurrentExpensesList();
        int index = IntStream.range(0, list.size()).filter(i -> list.get(i).getId().equals(oldCurrentExpenses.getId())).findFirst().orElse(-1);
        list.set(index, newCurrentExpenses);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void deleteCurrentExpenses(int position) {
        List<CurrentExpenses> list = getCurrentExpensesList();
        list.remove(position);
        fileManager.serialize(list, fullName);
    }

}
