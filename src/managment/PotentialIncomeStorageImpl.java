package managment;

import model.PotentialIncome;
import storage.PotentialIncomeStorage;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class PotentialIncomeStorageImpl implements PotentialIncomeStorage, Serializable {

    private final FileManager fileManager;

    public PotentialIncomeStorageImpl(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    static private final String fileName = "PotentialIncome.txt";
    static private final String dirName = "database";
    static private final String fullName = dirName + File.separator + fileName;

    @Override
    public void initPotentialIncome() {
        fileManager.createFiles(fullName, dirName);
    }

    @Override
    public ArrayList<PotentialIncome> getPotentialIncomeList() {
        return fileManager.deSerialize(fullName);
    }

    @Override
    public PotentialIncome getPotentialIncome(String id) {
        List<PotentialIncome> list = getPotentialIncomeList();
        return list.stream().filter(potentialExpenses -> potentialExpenses.getId().equals(id)).toList().getFirst();
    }

    @Override
    public PotentialIncome getPotentialIncomeByPositionInList(int potentialIncomePosition) {
        return getPotentialIncomeList().get(potentialIncomePosition);
    }

    @Override
    public void addPotentialIncome(PotentialIncome potentialIncome) {
        ArrayList<PotentialIncome> list = fileManager.deSerialize(fullName);
        list.add(potentialIncome);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void updatePotentialIncome(PotentialIncome oldPotentialIncome, PotentialIncome newPotentialIncome) {
        List<PotentialIncome> list = getPotentialIncomeList();
        int index = IntStream.range(0, list.size()).filter(i -> list.get(i).getId().equals(oldPotentialIncome.getId())).findFirst().orElse(-1);
        list.set(index, newPotentialIncome);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void deletePotentialIncome(int position) {
        List<PotentialIncome> list = getPotentialIncomeList();
        list.remove(position);
        fileManager.serialize(list, fullName);
    }

}
