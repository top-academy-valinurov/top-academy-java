package managment;

import model.Pocket;
import storage.PocketStorage;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class PocketStorageImpl implements PocketStorage, Serializable {

    private final FileManager fileManager;

    public PocketStorageImpl(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    static private final String fileName = "Pockets.txt";
    static private final String dirName = "database";
    static private final String fullName = dirName + File.separator + fileName;

    @Override
    public void initPockets() {
        fileManager.createFiles(fullName, dirName);
    }

    @Override
    public ArrayList<Pocket> getPocketList() {
        return fileManager.deSerialize(fullName);
    }

    @Override
    public Pocket getPocket(String pocketNumber) {
        List<Pocket> list = getPocketList();
        return list.stream().filter(pocket -> pocket.getPocketNumber().equals(pocketNumber)).toList().getFirst();
    }

    @Override
    public Pocket getPocketByPositionInList(int pocketPosition) {
        return getPocketList().get(pocketPosition);
    }

    @Override
    public void addPocket(Pocket pocket) {
        ArrayList<Pocket> list = fileManager.deSerialize(fullName);
        list.add(pocket);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void updatePocket(Pocket oldPocket, Pocket newPocket) {
        List<Pocket> list = getPocketList();
        int index = IntStream.range(0, list.size())
                .filter(i -> list.get(i).getPocketNumber().equals(oldPocket.getPocketNumber()))
                .findFirst()
                .orElse(-1);
        list.set(index, newPocket);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void deletePocket(int position) {
        List<Pocket> list = getPocketList();
        list.remove(position);
        fileManager.serialize(list, fullName);
    }

}
