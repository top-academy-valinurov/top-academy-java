package managment;

import model.CreditCard;
import storage.CreditCardStorage;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;


public class CreditCardStorageImpl implements CreditCardStorage, Serializable {

    private final FileManager fileManager;

    public CreditCardStorageImpl(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    static private final String fileName = "CreditCards.txt";
    static private final String dirName = "database";
    static private final String fullName = dirName + File.separator + fileName;

    @Override
    public void initCreditCards() {
        fileManager.createFiles(fullName, dirName);
    }

    @Override
    public ArrayList<CreditCard> getCreditCardList() {
        return fileManager.deSerialize(fullName);
    }

    @Override
    public CreditCard getCreditCard(String creditCardNumber) {
        List<CreditCard> list = getCreditCardList();
        return list.stream().filter(creditCard -> creditCard.getCreditCardNumber().equals(creditCardNumber)).toList().getFirst();
    }

    @Override
    public CreditCard getCreditCardByPositionInList(int creditCardPosition) {
        return getCreditCardList().get(creditCardPosition);
    }

    @Override
    public void addCreditCard(CreditCard creditCard) {
        ArrayList<CreditCard> list = fileManager.deSerialize(fullName);
        list.add(creditCard);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void updateCreditCard(CreditCard oldPocket, CreditCard newCreditCard) {
        List<CreditCard> list = getCreditCardList();
        int index = IntStream.range(0, list.size()).filter(i -> list.get(i).getCreditCardNumber().equals(oldPocket.getCreditCardNumber())).findFirst().orElse(-1);
        list.set(index, newCreditCard);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void deleteCreditCard(int position) {
        List<CreditCard> list = getCreditCardList();
        list.remove(position);
        fileManager.serialize(list, fullName);
    }

}
