package managment;

import model.PotentialExpenses;
import storage.PotentialExpensesStorage;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class PotentialExpensesStorageImpl implements PotentialExpensesStorage, Serializable {

    private final FileManager fileManager;

    public PotentialExpensesStorageImpl(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    static private final String fileName = "PotentialExpenses.txt";
    static private final String dirName = "database";
    static private final String fullName = dirName + File.separator + fileName;

    @Override
    public void initPotentialExpenses() {
        fileManager.createFiles(fullName, dirName);
    }

    @Override
    public ArrayList<PotentialExpenses> getPotentialExpensesList() {
        return fileManager.deSerialize(fullName);
    }

    @Override
    public PotentialExpenses getPotentialExpenses(String id) {
        List<PotentialExpenses> list = getPotentialExpensesList();
        return list.stream().filter(potentialExpenses -> potentialExpenses.getId().equals(id)).toList().getFirst();
    }

    @Override
    public PotentialExpenses getPotentialExpensesByPositionInList(int potentialExpensesPosition) {
        return getPotentialExpensesList().get(potentialExpensesPosition);
    }

    @Override
    public void addPotentialExpenses(PotentialExpenses potentialExpenses) {
        ArrayList<PotentialExpenses> list = fileManager.deSerialize(fullName);
        list.add(potentialExpenses);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void updatePotentialExpenses(PotentialExpenses oldPotentialExpenses, PotentialExpenses newPotentialExpenses) {
        List<PotentialExpenses> list = getPotentialExpensesList();
        int index = IntStream.range(0, list.size()).filter(i -> list.get(i).getId().equals(oldPotentialExpenses.getId())).findFirst().orElse(-1);
        list.set(index, newPotentialExpenses);
        fileManager.serialize(list, fullName);
    }

    @Override
    public void deletePotentialExpenses(int position) {
        List<PotentialExpenses> list = getPotentialExpensesList();
        list.remove(position);
        fileManager.serialize(list, fullName);
    }

}
