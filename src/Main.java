import managment.*;
import model.*;
import viewmodels.InfoViewModel;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.Scanner;

import static java.lang.Math.random;

public class Main {
    static InfoViewModel viewModel;
    static Scanner scanner;
    static FileManager fileManager;

    public static void main(String[] args) {
        init();
        showMainMenu();
    }

    private static void init() {
        fileManager = new FileManager();
        scanner = new Scanner(System.in);
        viewModel = new InfoViewModel(
                new PocketStorageImpl(fileManager),
                new CreditCardStorageImpl(fileManager),
                new CurrentExpensesStorageImpl(fileManager),
                new PotentialExpensesStorageImpl(fileManager),
                new PotentialIncomeStorageImpl(fileManager)
        );
        viewModel.init();
    }

    private static void showMainMenu() {
        System.out.println("""
                Выберите какие действия хотите произвести (введите номер пункта):
                0. Сгенирировать данные.
                1. Показать меню кошельков.
                2. Показать меню кредитных карт.
                3. Показать доступный баланс.
                4. Показать текущие расходы.
                5. Показать потенциальные расходы.
                6. Показать потенциальные доходы.
                9. Выйти.
                """);
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showPocketMenu();
                break;
            }
            case 2: {
                showCreditCardMenu();
                break;
            }
            case 3: {
                showFullBalance();
                break;
            }
            case 4: {
                showCurrentExpensesMenu();
                break;
            }
            case 5: {
                showPotentialExpensesMenu();
                break;
            }
            case 6: {
                showPotentialIncomeMenu();
                break;
            }
            case 9: {
                System.exit(0);
                break;
            }
            case 0: {
                generateData();
                break;
            }
            default: {
            }
        }
    }

    private static void showPotentialIncomeMenu() {
        System.out.println("""
                Существуют потенциальные доходы: "Сумма дохода" | "Номер кошелька пополнения" | "Номер кредитной карты пополнения" | "Комментарий"
                """);
        List<PotentialIncome> list = viewModel.getPPotentialIncomeList();
        for (int i = 0; i < list.size(); i++) {
            PotentialIncome potentialIncome = list.get(i);
            int position = i + 1;
            System.out.println(position + ") " + potentialIncome.getAmount() + " | " + (potentialIncome.getPocketNumber().isEmpty() ? "****" : potentialIncome.getPocketNumber()) + " | " + (potentialIncome.getCardNumber().isEmpty() ? "****" : potentialIncome.getCardNumber()) + " | " + potentialIncome.getComment());
        }
        System.out.println();
        System.out.println("""
                Выберите какие действия хотите произвести с Потенциальными доходами:
                1. Редактировать потенциальные доходы.
                2. Удалить потенциальные доходы.
                9. Вернуться
                """);
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showUpdatePotentialIncomeMenu();
                break;
            }
            case 2: {
                showMenuDeletePotentialIncome();
                break;
            }
            case 9: {
                showMainMenu();
                break;
            }
            default: {
            }
        }
    }

    private static void showPotentialExpensesMenu() {
        System.out.println("""
                Существуют потенциальные расходы: "Сумма рассхода" | "Номер кошелька списания" | "Номер кредитной карты списания" | "Комментарий"
                """);
        List<PotentialExpenses> list = viewModel.getPotentialExpensesList();
        for (int i = 0; i < list.size(); i++) {
            PotentialExpenses potentialExpenses = list.get(i);
            int position = i + 1;
            System.out.println(position + ") " + potentialExpenses.getAmount() + " | " + (potentialExpenses.getPocketNumber().isEmpty() ? "****" : potentialExpenses.getPocketNumber()) + " | " + (potentialExpenses.getCardNumber().isEmpty() ? "****" : potentialExpenses.getCardNumber()) + " | " + potentialExpenses.getComment());
        }
        System.out.println();
        System.out.println("""
                Выберите какие действия хотите произвести с Потенциальными расходами:
                1. Редактировать потенциальные расходы.
                2. Удалить потенциальные расходы.
                9. Вернуться
                """);
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showUpdatePotentialExpensesMenu();
                break;
            }
            case 2: {
                showMenuDeletePotentialExpenses();
                break;
            }
            case 9: {
                showMainMenu();
                break;
            }
            default: {
            }
        }
    }

    private static void showCurrentExpensesMenu() {
        System.out.println("""
                Существуют текущие расходы: "Сумма рассхода" | "Номер кошелька списания" | "Номер кредитной карты списания" | "Комментарий"
                """);
        List<CurrentExpenses> list = viewModel.getCurrentExpensesList();
        for (int i = 0; i < list.size(); i++) {
            CurrentExpenses currentExpenses = list.get(i);
            int position = i + 1;
            System.out.println(position + ") " + currentExpenses.getAmount() + " | " + (currentExpenses.getPocketNumber().isEmpty() ? "****" : currentExpenses.getPocketNumber()) + " | " + (currentExpenses.getCardNumber().isEmpty() ? "****" : currentExpenses.getCardNumber()) + " | " + currentExpenses.getComment());
        }
        System.out.println();
        System.out.println("""
                Выберите какие действия хотите произвести с Текущими расходами:
                1. Редактировать текущие расходы.
                2. Удалить текушие расходы.
                9. Вернуться
                """);
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showUpdateCurrentExpensesMenu();
                break;
            }
            case 2: {
                showMenuDeleteCurrentExpenses();
                break;
            }
            case 9: {
                showMainMenu();
                break;
            }
            default: {
            }
        }
    }

    private static void showFullBalance() {
        List<CreditCard> creditCardList = viewModel.getCreditCardsList();
        List<Pocket> pocketList = viewModel.getPocketList();
        BigDecimal creditCardBalance = BigDecimal.ZERO;
        BigDecimal pocketBalance = BigDecimal.ZERO;
        BigDecimal fullBalance = BigDecimal.ZERO;
        for (CreditCard creditCard : creditCardList) {
            creditCardBalance = creditCardBalance.add(creditCard.getBalance(), new MathContext(6, RoundingMode.HALF_UP));
        }
        for (Pocket pocket : pocketList) {
            pocketBalance = pocketBalance.add(pocket.getBalance(), new MathContext(6, RoundingMode.HALF_UP));
        }
        fullBalance = fullBalance.add(pocketBalance).add(creditCardBalance);
        System.out.println(
                "Баланс на кредитных картах: " + creditCardBalance + "\n" +
                        "Баланс на кошельках: " + pocketBalance + "\n" +
                        "Общий баланс: " + fullBalance + "\n"
        );
        showMainMenu();
    }

    private static void showPocketMenu() {
        System.out.println(""" 
                Существуют кошельки: "Имя кошелька" | "Баланс" | "Номер кошелька"
                """);
        List<Pocket> list = viewModel.getPocketList();
        for (int i = 0; i < list.size(); i++) {
            Pocket pocket = list.get(i);
            int position = i + 1;
            System.out.println(position + ") " + pocket.getPocketName() + " | " + pocket.getBalance() + " | " + pocket.getPocketNumber());
        }
        System.out.println();
        System.out.println("""
                Выберите какие действия хотите произвести с кошельками:
                1. Редактировать кошелёк.
                2. Удалить кошелёк.
                9. Вернуться""");
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showUpdatePocketMenu();
                break;
            }
            case 2: {
                showMenuDeletePocket();
                break;
            }
            case 9: {
                showMainMenu();
                break;
            }
            default: {
            }
        }
    }

    private static void showCreditCardMenu() {
        System.out.println("""
                Существуют кредитные карты: "Имя кредитной карты" | "Баланс" | "Номер кредитной карты"
                """);
        List<CreditCard> list = viewModel.getCreditCardsList();
        for (int i = 0; i < list.size(); i++) {
            CreditCard creditCard = list.get(i);
            int position = i + 1;
            System.out.println(position + ") " + creditCard.getCreditCardName() + " | " + creditCard.getBalance() + " | " + creditCard.getCreditCardNumber());
        }
        System.out.println();
        System.out.println("""
                Выберите какие действия хотите произвести с кредитной картой:
                1. Редактировать кредитную карту.
                2. Удалить кредитную карту.
                9. Вернуться""");
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showUpdateCreditCardMenu();
                break;
            }
            case 2: {
                showMenuDeleteCreditCard();
                break;
            }
            case 9: {
                showMainMenu();
                break;
            }
            default: {
            }
        }
    }

    private static void showUpdatePocketMenu() {
        System.out.println("Укажите порядковый номер кошелька, которой хотите изменить");
        int pocketPosition = scanner.nextInt() - 1;
        System.out.println("Укажите что хотите изменить у кошелька под порядковым номером " + pocketPosition + ":\n" + "1. Редактировать имя. \n" + "2. Редактировать баланс.");
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showChangePocketNameDialog(pocketPosition);
                break;
            }
            case 2: {
                showChangePocketBalanceDialog(pocketPosition);
                break;
            }
            default: {
            }

        }
    }

    private static void showUpdatePotentialIncomeMenu() {
        System.out.println("Укажите порядковый номер потенциальных доходов, которой хотите изменить");
        int currentPotentialIncomePosition = scanner.nextInt() - 1;
        System.out.println("Укажите что хотите изменить у потенциальных доходов под порядковым номером " + currentPotentialIncomePosition + ":\n" +
                "1. Редактировать привязку доходов (кошелек/кредитная карта).\n" +
                "2. Редактировать баланс.\n" +
                "3. Редактировать комментарий\n");
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showChangePotentialIncomeBindingDialog(currentPotentialIncomePosition);
                break;
            }
            case 2: {
                showChangePotentialIncomeBalanceDialog(currentPotentialIncomePosition);
                break;
            }
            case 3: {
                showChangePotentialIncomeCommentDialog(currentPotentialIncomePosition);
                break;
            }
            default: {
            }
        }
    }

    private static void showUpdatePotentialExpensesMenu() {
        System.out.println("Укажите порядковый номер потенциальных расходов, которой хотите изменить");
        int currentExpensesPosition = scanner.nextInt() - 1;
        System.out.println("Укажите что хотите изменить у потенциальных расходов под порядковым номером " + currentExpensesPosition + ":\n" +
                "1. Редактировать привязку расходов (кошелек/кредитная карта).\n" +
                "2. Редактировать баланс.\n" +
                "3. Редактировать комментарий\n");
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showChangePotentialExpensesBindingDialog(currentExpensesPosition);
                break;
            }
            case 2: {
                showChangePotentialExpensesBalanceDialog(currentExpensesPosition);
                break;
            }
            case 3: {
                showChangePotentialExpensesCommentDialog(currentExpensesPosition);
                break;
            }
            default: {
            }
        }
    }

    private static void showUpdateCurrentExpensesMenu() {
        System.out.println("Укажите порядковый номер текущих расходов, которой хотите изменить");
        int currentExpensesPosition = scanner.nextInt() - 1;
        System.out.println("Укажите что хотите изменить у текущих расходов под порядковым номером " + currentExpensesPosition + ":\n" +
                "1. Редактировать привязку расходов (кошелек/кредитная карта).\n" +
                "2. Редактировать баланс.\n" +
                "3. Редактировать комментарий\n");
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showChangeCurrentExpensesBindingDialog(currentExpensesPosition);
                break;
            }
            case 2: {
                showChangeCurrentExpensesBalanceDialog(currentExpensesPosition);
                break;
            }
            case 3: {
                showChangeCurrentExpensesCommentDialog(currentExpensesPosition);
                break;
            }
            default: {
            }
        }
    }

    private static void showChangePotentialIncomeCommentDialog(int currentPotentialExpensesPosition) {
        System.out.println("Введите комментарий для потенциальных расходов:");
        String newComment = scanner.next();
        viewModel.updatePotentialIncomeComment(currentPotentialExpensesPosition, newComment);
        showPotentialIncomeMenu();
    }

    private static void showChangePotentialExpensesCommentDialog(int currentPotentialExpensesPosition) {
        System.out.println("Введите комментарий для потенциальных расходов:");
        String newComment = scanner.next();
        viewModel.updatePotentialExpensesComment(currentPotentialExpensesPosition, newComment);
        showPotentialExpensesMenu();
    }

    private static void showChangeCurrentExpensesCommentDialog(int currentExpensesPosition) {
        System.out.println("Введите комментарий для текущих расходов:");
        String newComment = scanner.next();
        viewModel.updateCurrentExpensesComment(currentExpensesPosition, newComment);
        showCurrentExpensesMenu();
    }

    private static void showChangePotentialIncomeBalanceDialog(int currentPotentialExpensesPosition) {
        System.out.println("""
                Ввведите как изменились траты потенциальных доходов:
                1. Увеличились.
                2. Уменьшились.
                """);
        boolean isPlus = scanner.nextInt() == 1;
        System.out.println("Введите на какую сумму изменились траты потенциальных доходов (разделитель \",\"):");
        BigDecimal updateBalanceValue = scanner.nextBigDecimal();
        viewModel.updatePotentialIncomeAmount(currentPotentialExpensesPosition, updateBalanceValue, isPlus);
        showPotentialIncomeMenu();
    }

    private static void showChangePotentialExpensesBalanceDialog(int currentPotentialExpensesPosition) {
        System.out.println("""
                Ввведите как изменились траты потенциальных расходов:
                1. Увеличились.
                2. Уменьшились.
                """);
        boolean isPlus = scanner.nextInt() == 1;
        System.out.println("Введите на какую сумму изменились траты потенциальных расходов (разделитель \",\"):");
        BigDecimal updateBalanceValue = scanner.nextBigDecimal();
        viewModel.updatePotentialExpensesAmount(currentPotentialExpensesPosition, updateBalanceValue, isPlus);
        showPotentialExpensesMenu();
    }

    private static void showChangeCurrentExpensesBalanceDialog(int currentExpensesPosition) {
        System.out.println("""
                Ввведите как изменились траты текущих расходов:
                1. Увеличились.
                2. Уменьшились.
                """);
        boolean isPlus = scanner.nextInt() == 1;
        System.out.println("Введите на какую сумму изменились траты текущих расходов (разделитель \",\"):");
        BigDecimal updateBalanceValue = scanner.nextBigDecimal();
        viewModel.updateCurrentExpensesAmount(currentExpensesPosition, updateBalanceValue, isPlus);
        showCurrentExpensesMenu();
    }

    private static void showUpdateCreditCardMenu() {
        System.out.println("Укажите порядковый номер кредитной карты, которую хотите изменить");
        int creditCardPosition = scanner.nextInt() - 1;
        System.out.println("Укажите что хотите изменить у кредитной карты под порядковым номером " + creditCardPosition + ":\n" + "1. Редактировать имя. \n" + "2. Редактировать баланс.");
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showChangeCreditCardNameDialog(creditCardPosition);
                break;
            }
            case 2: {
                showChangeCreditCardBalanceDialog(creditCardPosition);
                break;
            }
            default: {
            }
        }
    }

    private static void showChangePocketNameDialog(int pocketPosition) {
        System.out.println("Ввведите новое имя кошелька:");
        String newName = scanner.next();
        viewModel.updatePocketName(pocketPosition, newName);
        showPocketMenu();
    }

    private static void showChangePotentialIncomeBindingDialog(int currentPotentialIncomePosition) {
        System.out.print("Потенциальные доходы привязаны к ");
        List<PotentialIncome> list = viewModel.getPPotentialIncomeList();
        PotentialIncome potentialIncome = list.get(currentPotentialIncomePosition);
        if (potentialIncome.getPocketNumber().isEmpty()) {
            CreditCard creditCard = viewModel.getCreditCard(potentialIncome.getCardNumber());
            System.out.println("кредитной карте с номером: " + creditCard.getCreditCardNumber() + ", баланс: " + creditCard.getBalance());
        } else {
            Pocket pocket = viewModel.getPocket(potentialIncome.getPocketNumber());
            System.out.println("кошельку с номером: " + pocket.getPocketNumber() + ", баланс: " + pocket.getBalance());
        }
        System.out.println("""
                К какому источнику привязать доходы?
                1. Кошелёк.
                2. Кредитная карта.
                """);
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showChangePotentialIncomeBindingToPocketDialog(potentialIncome.getId());
                break;
            }
            case 2: {
                showChangePotentialIncomeBindingToCreditCardDialog(potentialIncome.getId());
                break;
            }
            default: {
            }
        }
    }

    private static void showChangePotentialExpensesBindingDialog(int currentPotentialExpensesPosition) {
        System.out.print("Потенциальные расходы привязаны к ");
        List<PotentialExpenses> list = viewModel.getPotentialExpensesList();
        PotentialExpenses potentialExpenses = list.get(currentPotentialExpensesPosition);
        if (potentialExpenses.getPocketNumber().isEmpty()) {
            CreditCard creditCard = viewModel.getCreditCard(potentialExpenses.getCardNumber());
            System.out.println("кредитной карте с номером: " + creditCard.getCreditCardNumber() + ", баланс: " + creditCard.getBalance());
        } else {
            Pocket pocket = viewModel.getPocket(potentialExpenses.getPocketNumber());
            System.out.println("кошельку с номером: " + pocket.getPocketNumber() + ", баланс: " + pocket.getBalance());
        }
        System.out.println("""
                К какому источнику привязать расходы?
                1. Кошелёк.
                2. Кредитная карта.
                """);
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showChangePotentialExpensesBindingToPocketDialog(potentialExpenses.getId());
                break;
            }
            case 2: {
                showChangePotentialExpensesBindingToCreditCardDialog(potentialExpenses.getId());
                break;
            }
            default: {
            }
        }
    }

    private static void showChangeCurrentExpensesBindingDialog(int currentExpensesPosition) {
        System.out.print("Текущие расходы привязаны к ");
        List<CurrentExpenses> list = viewModel.getCurrentExpensesList();
        CurrentExpenses currentExpenses = list.get(currentExpensesPosition);
        if (currentExpenses.getPocketNumber().isEmpty()) {
            CreditCard creditCard = viewModel.getCreditCard(currentExpenses.getCardNumber());
            System.out.println("кредитной карте с номером: " + creditCard.getCreditCardNumber() + ", баланс: " + creditCard.getBalance());
        } else {
            Pocket pocket = viewModel.getPocket(currentExpenses.getPocketNumber());
            System.out.println("кошельку с номером: " + pocket.getPocketNumber() + ", баланс: " + pocket.getBalance());
        }
        System.out.println("""
                К какому источнику привязать расходы?
                1. Кошелёк.
                2. Кредитная карта.
                """);
        int choosePosition = scanner.nextInt();
        switch (choosePosition) {
            case 1: {
                showChangeCurrentExpensesBindingToPocketDialog(currentExpenses.getId());
                break;
            }
            case 2: {
                showChangeCurrentExpensesBindingToCreditCardDialog(currentExpenses.getId());
                break;
            }
            default: {
            }
        }
    }

    private static void showChangePotentialIncomeBindingToCreditCardDialog(String currentPotentialExpensesId) {
        List<CreditCard> creditCardList = viewModel.getCreditCardsList();
        System.out.println("Имеются кредитные карты (\"Номер карты\" | \"Баланс\"):");
        for (int i = 0; i < creditCardList.size(); i++) {
            CreditCard creditCard = creditCardList.get(i);
            int currentPosition = i + 1;
            System.out.println(currentPosition + ") " + creditCard.getCreditCardNumber() + " | " + creditCard.getBalance());
        }
        System.out.println("Выбери порядковый номер кредитной карты к которой вы хотите привязать текущие доходы:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.updatePotentialIncomeBinding(currentPotentialExpensesId, "", creditCardList.get(choosePosition).getCreditCardNumber());
        showPotentialIncomeMenu();
    }

    private static void showChangePotentialExpensesBindingToCreditCardDialog(String currentPotentialExpensesId) {
        List<CreditCard> creditCardList = viewModel.getCreditCardsList();
        System.out.println("Имеются кредитные карты (\"Номер карты\" | \"Баланс\"):");
        for (int i = 0; i < creditCardList.size(); i++) {
            CreditCard creditCard = creditCardList.get(i);
            int currentPosition = i + 1;
            System.out.println(currentPosition + ") " + creditCard.getCreditCardNumber() + " | " + creditCard.getBalance());
        }
        System.out.println("Выбери порядковый номер кредитной карты к которой вы хотите привязать текущие расходы:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.updatePotentialExpensesBinding(currentPotentialExpensesId, "", creditCardList.get(choosePosition).getCreditCardNumber());
        showPotentialExpensesMenu();
    }

    private static void showChangeCurrentExpensesBindingToCreditCardDialog(String currentExpensesId) {
        List<CreditCard> creditCardList = viewModel.getCreditCardsList();
        System.out.println("Имеются кредитные карты (\"Номер карты\" | \"Баланс\"):");
        for (int i = 0; i < creditCardList.size(); i++) {
            CreditCard creditCard = creditCardList.get(i);
            int currentPosition = i + 1;
            System.out.println(currentPosition + ") " + creditCard.getCreditCardNumber() + " | " + creditCard.getBalance());
        }
        System.out.println("Выбери порядковый номер кредитной карты к которой вы хотите привязать текущие расходы:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.updateCurrentExpensesBinding(currentExpensesId, "", creditCardList.get(choosePosition).getCreditCardNumber());
        showCurrentExpensesMenu();
    }

    private static void showChangePotentialIncomeBindingToPocketDialog(String currentPotentialIncomeId) {
        List<Pocket> pocketList = viewModel.getPocketList();
        System.out.println("Имеются кошельки (\"Номер кошелька\" | \"Баланс\"):");
        for (int i = 0; i < pocketList.size(); i++) {
            Pocket pocket = pocketList.get(i);
            int currentPosition = i + 1;
            System.out.println(currentPosition + ") " + pocket.getPocketNumber() + " | " + pocket.getBalance());
        }
        System.out.println("Выбери порядковый номер кошелька к которой вы хотите привязать текущие доходы:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.updatePotentialIncomeBinding(currentPotentialIncomeId, pocketList.get(choosePosition).getPocketNumber(), "");
        showPotentialIncomeMenu();
    }

    private static void showChangePotentialExpensesBindingToPocketDialog(String currentPotentialExpensesId) {
        List<Pocket> pocketList = viewModel.getPocketList();
        System.out.println("Имеются кошельки (\"Номер кошелька\" | \"Баланс\"):");
        for (int i = 0; i < pocketList.size(); i++) {
            Pocket pocket = pocketList.get(i);
            int currentPosition = i + 1;
            System.out.println(currentPosition + ") " + pocket.getPocketNumber() + " | " + pocket.getBalance());
        }
        System.out.println("Выбери порядковый номер кошелька к которой вы хотите привязать текущие расходы:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.updatePotentialExpensesBinding(currentPotentialExpensesId, pocketList.get(choosePosition).getPocketNumber(), "");
        showPotentialExpensesMenu();
    }

    private static void showChangeCurrentExpensesBindingToPocketDialog(String currentExpensesId) {
        List<Pocket> pocketList = viewModel.getPocketList();
        System.out.println("Имеются кошельки (\"Номер кошелька\" | \"Баланс\"):");
        for (int i = 0; i < pocketList.size(); i++) {
            Pocket pocket = pocketList.get(i);
            int currentPosition = i + 1;
            System.out.println(currentPosition + ") " + pocket.getPocketNumber() + " | " + pocket.getBalance());
        }
        System.out.println("Выбери порядковый номер кошелька к которой вы хотите привязать текущие расходы:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.updateCurrentExpensesBinding(currentExpensesId, pocketList.get(choosePosition).getPocketNumber(), "");
        showCurrentExpensesMenu();
    }

    private static void showChangeCreditCardNameDialog(int creditCardPosition) {
        System.out.println("Ввведите новое имя кредитной карты:");
        String newName = scanner.next();
        viewModel.updateCreditCardName(creditCardPosition, newName);
        showCreditCardMenu();
    }

    private static void showChangePocketBalanceDialog(int pocketPosition) {
        System.out.println("""
                Ввведите как изменился баланс кошелька:
                1. Увеличился.
                2. Уменьшился.""");
        boolean isPlus = scanner.nextInt() == 1;
        System.out.println("Введите на какую сумму изменился баланс кошелька (разделитель \",\"):");
        BigDecimal updateBalanceValue = scanner.nextBigDecimal();
        viewModel.updatePocketBalance(pocketPosition, updateBalanceValue, isPlus);
        showPocketMenu();
    }

    private static void showChangeCreditCardBalanceDialog(int creditCardPosition) {
        System.out.println("""
                Ввведите как изменился баланс кредитной карты:
                1. Увеличился.
                2. Уменьшился.""");
        boolean isPlus = scanner.nextInt() == 1;
        System.out.println("Введите на какую сумму изменился баланс кредитной карты:");
        BigDecimal updateBalanceValue = scanner.nextBigDecimal();
        viewModel.updateCreditCardBalance(creditCardPosition, updateBalanceValue, isPlus);
        showCreditCardMenu();
    }

    private static void showMenuDeletePotentialIncome() {
        System.out.println("Укажите порядковый номер потенциальных доходов, которой хотите удалить:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.deletePotentialIncome(choosePosition);
        System.out.println("Потенциальные доходы с порядковым номером " + choosePosition + " удалёны");
        showPotentialIncomeMenu();
    }

    private static void showMenuDeletePotentialExpenses() {
        System.out.println("Укажите порядковый номер потенциальных расходов, которой хотите удалить:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.deletePotentialExpenses(choosePosition);
        System.out.println("Потенциальные расходы с порядковым номером " + choosePosition + " удалёны");
        showPotentialExpensesMenu();
    }

    private static void showMenuDeleteCurrentExpenses() {
        System.out.println("Укажите порядковый номер текущих расходов, которой хотите удалить:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.deleteCurrentExpenses(choosePosition);
        System.out.println("Текущие расходы с порядковым номером " + choosePosition + " удалёны");
        showCurrentExpensesMenu();
    }

    private static void showMenuDeletePocket() {
        System.out.println("Укажите порядковый номер кошелька, которой хотите удалить:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.deletePocket(choosePosition);
        System.out.println("Кошелёк с порядковым номером " + choosePosition + " удалён");
        showPocketMenu();
    }

    private static void showMenuDeleteCreditCard() {
        System.out.println("Укажите порядковый номер кредитной карты, которую хотите удалить:");
        int choosePosition = scanner.nextInt() - 1;
        viewModel.deleteCreditCard(choosePosition);
        System.out.println("Кредитная карта с порядковым номером " + choosePosition + " удалён");
        showPocketMenu();
    }

    private static void generateData() {
        Pocket testPocket1 = new Pocket("testPocket1", BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP));
        Pocket testPocket2 = new Pocket("testPocket2", BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP));
        Pocket testPocket3 = new Pocket("testPocket3", BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP));
        Pocket testPocket4 = new Pocket("testPocket4", BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP));
        viewModel.addPocket(testPocket1);
        viewModel.addPocket(testPocket2);
        viewModel.addPocket(testPocket3);
        viewModel.addPocket(testPocket4);
        CreditCard creditCard1 = new CreditCard("creditCard1", BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP));
        CreditCard creditCard2 = new CreditCard("creditCard2", BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP));
        CreditCard creditCard3 = new CreditCard("creditCard3", BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP));
        CreditCard creditCard4 = new CreditCard("creditCard4", BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP));
        viewModel.addCreditCard(creditCard1);
        viewModel.addCreditCard(creditCard2);
        viewModel.addCreditCard(creditCard3);
        viewModel.addCreditCard(creditCard4);
        CurrentExpenses currentExpenses1 = new CurrentExpenses(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), testPocket1.getPocketNumber(), "", "списание с testPocket1");
        CurrentExpenses currentExpenses2 = new CurrentExpenses(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), testPocket3.getPocketNumber(), "", "списание с testPocket3");
        CurrentExpenses currentExpenses3 = new CurrentExpenses(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), "", creditCard2.getCreditCardNumber(), "списание с creditCard2");
        CurrentExpenses currentExpenses4 = new CurrentExpenses(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), "", creditCard4.getCreditCardNumber(), "списание с creditCard4");
        viewModel.addCurrentExpenses(currentExpenses1);
        viewModel.addCurrentExpenses(currentExpenses2);
        viewModel.addCurrentExpenses(currentExpenses3);
        viewModel.addCurrentExpenses(currentExpenses4);
        PotentialExpenses potentialExpenses1 = new PotentialExpenses(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), testPocket1.getPocketNumber(), "", "списание с testPocket1");
        PotentialExpenses potentialExpenses2 = new PotentialExpenses(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), testPocket3.getPocketNumber(), "", "списание с testPocket3");
        PotentialExpenses potentialExpenses3 = new PotentialExpenses(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), "", creditCard2.getCreditCardNumber(), "списание с creditCard2");
        PotentialExpenses potentialExpenses4 = new PotentialExpenses(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), "", creditCard4.getCreditCardNumber(), "списание с creditCard4");
        viewModel.addPotentialExpenses(potentialExpenses1);
        viewModel.addPotentialExpenses(potentialExpenses2);
        viewModel.addPotentialExpenses(potentialExpenses3);
        viewModel.addPotentialExpenses(potentialExpenses4);
        PotentialIncome potentialIncome1 = new PotentialIncome(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), testPocket1.getPocketNumber(), "", "списание с testPocket1");
        PotentialIncome potentialIncome2 = new PotentialIncome(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), testPocket3.getPocketNumber(), "", "списание с testPocket3");
        PotentialIncome potentialIncome3 = new PotentialIncome(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), "", creditCard2.getCreditCardNumber(), "списание с creditCard2");
        PotentialIncome potentialIncome4 = new PotentialIncome(BigDecimal.valueOf(random() * 10000).setScale(2, RoundingMode.HALF_UP), "", creditCard4.getCreditCardNumber(), "списание с creditCard4");
        viewModel.addPotentialIncome(potentialIncome1);
        viewModel.addPotentialIncome(potentialIncome2);
        viewModel.addPotentialIncome(potentialIncome3);
        viewModel.addPotentialIncome(potentialIncome4);
        System.out.println("Данные сгенерированы.");
        showMainMenu();
    }

}