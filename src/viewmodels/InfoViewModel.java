package viewmodels;

import model.*;
import storage.*;

import java.math.BigDecimal;
import java.util.ArrayList;

public class InfoViewModel {

    public InfoViewModel(
            PocketStorage pocketStorage,
            CreditCardStorage creditCardStorage,
            CurrentExpensesStorage currentExpensesStorage,
            PotentialExpensesStorage potentialExpensesStorage,
            PotentialIncomeStorage potentialIncomeStorage) {
        this.pocketStorage = pocketStorage;
        this.creditCardStorage = creditCardStorage;
        this.currentExpensesStorage = currentExpensesStorage;
        this.potentialExpensesStorage = potentialExpensesStorage;
        this.potentialIncomeStorage = potentialIncomeStorage;
    }

    private final PocketStorage pocketStorage;
    private final CreditCardStorage creditCardStorage;
    private final CurrentExpensesStorage currentExpensesStorage;
    private final PotentialExpensesStorage potentialExpensesStorage;
    private final PotentialIncomeStorage potentialIncomeStorage;

    public void init() {
        pocketStorage.initPockets();
        creditCardStorage.initCreditCards();
        currentExpensesStorage.initCurrentExpenses();
        potentialExpensesStorage.initPotentialExpenses();
        potentialIncomeStorage.initPotentialIncome();
    }

    public void addPocket(Pocket pocket) {
        pocketStorage.addPocket(pocket);
    }

    public Pocket getPocket(String pocketNumber) {
        return pocketStorage.getPocket(pocketNumber);
    }

    public ArrayList<Pocket> getPocketList() {
        return pocketStorage.getPocketList();
    }

    public void updatePocketName(int pocketPosition, String newName) {
        Pocket oldPocket = pocketStorage.getPocketByPositionInList(pocketPosition);
        Pocket newPocket = oldPocket.clone();
        newPocket.setPocketName(newName);
        pocketStorage.updatePocket(oldPocket, newPocket);
    }

    public void updatePocketBalance(int pocketPosition, BigDecimal updateBalanceValue, boolean isPlus) {
        Pocket oldPocket = pocketStorage.getPocketByPositionInList(pocketPosition);
        Pocket newPocket = oldPocket.clone();
        if (isPlus) {
            newPocket.setBalance(oldPocket.getBalance().add(updateBalanceValue));
        } else {
            newPocket.setBalance(oldPocket.getBalance().subtract(updateBalanceValue));
        }
        pocketStorage.updatePocket(oldPocket, newPocket);
    }

    public void deletePocket(int position) {
        pocketStorage.deletePocket(position);
    }

    public void addCreditCard(CreditCard creditCard) {
        creditCardStorage.addCreditCard(creditCard);
    }

    public CreditCard getCreditCard(String creditCardNumber) {
        return creditCardStorage.getCreditCard(creditCardNumber);
    }

    public ArrayList<CreditCard> getCreditCardsList() {
        return creditCardStorage.getCreditCardList();
    }

    public void updateCreditCardName(int creditCardPosition, String newName) {
        CreditCard oldCreditCard = creditCardStorage.getCreditCardByPositionInList(creditCardPosition);
        CreditCard newCreditCard = oldCreditCard.clone();
        newCreditCard.setPocketName(newName);
        creditCardStorage.updateCreditCard(oldCreditCard, newCreditCard);
    }

    public void updateCreditCardBalance(int creditCardPosition, BigDecimal updateBalanceValue, boolean isPlus) {
        CreditCard oldCreditCard = creditCardStorage.getCreditCardByPositionInList(creditCardPosition);
        CreditCard newCreditCard = oldCreditCard.clone();
        if (isPlus) {
            newCreditCard.setBalance(oldCreditCard.getBalance().add(updateBalanceValue));
        } else {
            newCreditCard.setBalance(oldCreditCard.getBalance().subtract(updateBalanceValue));
        }
        creditCardStorage.updateCreditCard(oldCreditCard, newCreditCard);
    }

    public void deleteCreditCard(int position) {
        creditCardStorage.deleteCreditCard(position);
    }

    public void addPotentialIncome(PotentialIncome potentialIncome) {
        potentialIncomeStorage.addPotentialIncome(potentialIncome);
    }

    public ArrayList<PotentialIncome> getPPotentialIncomeList() {
        return potentialIncomeStorage.getPotentialIncomeList();
    }

    public void updatePotentialIncomeComment(int potentialIncomePosition, String newComment) {
        PotentialIncome oldPotentialIncome = potentialIncomeStorage.getPotentialIncomeByPositionInList(potentialIncomePosition);
        PotentialIncome newPotentialIncome = oldPotentialIncome.clone();
        newPotentialIncome.setComment(newComment);
        potentialIncomeStorage.updatePotentialIncome(oldPotentialIncome, newPotentialIncome);
    }

    public void updatePotentialIncomeAmount(int currentPotentialIncome, BigDecimal updateAmountValue, boolean isPlus) {
        PotentialIncome oldPotentialIncome = potentialIncomeStorage.getPotentialIncomeByPositionInList(currentPotentialIncome);
        PotentialIncome newPotentialIncome = oldPotentialIncome.clone();
        if (isPlus) {
            newPotentialIncome.setAmount(oldPotentialIncome.getAmount().add(updateAmountValue));
        } else {
            newPotentialIncome.setAmount(oldPotentialIncome.getAmount().subtract(updateAmountValue));
        }
        potentialIncomeStorage.updatePotentialIncome(oldPotentialIncome, newPotentialIncome);
    }

    public void updatePotentialIncomeBinding(String currentPotentialIncomeId, String newPocketNumber, String newCreditCardNumber) {
        PotentialIncome oldPotentialIncome = potentialIncomeStorage.getPotentialIncome(currentPotentialIncomeId);
        PotentialIncome newPotentialIncome = oldPotentialIncome.clone();
        newPotentialIncome.setCardNumber(newCreditCardNumber);
        newPotentialIncome.setPocketNumber(newPocketNumber);
        potentialIncomeStorage.updatePotentialIncome(oldPotentialIncome, newPotentialIncome);
    }

    public void deletePotentialIncome(int position) {
        potentialIncomeStorage.deletePotentialIncome(position);
    }

    public void addPotentialExpenses(PotentialExpenses potentialExpenses) {
        potentialExpensesStorage.addPotentialExpenses(potentialExpenses);
    }

    public ArrayList<PotentialExpenses> getPotentialExpensesList() {
        return potentialExpensesStorage.getPotentialExpensesList();
    }

    public void updatePotentialExpensesComment(int potentialExpensesPosition, String newComment) {
        PotentialExpenses oldPotentialExpenses = potentialExpensesStorage.getPotentialExpensesByPositionInList(potentialExpensesPosition);
        PotentialExpenses newPotentialExpenses = oldPotentialExpenses.clone();
        newPotentialExpenses.setComment(newComment);
        potentialExpensesStorage.updatePotentialExpenses(oldPotentialExpenses, newPotentialExpenses);
    }

    public void updatePotentialExpensesAmount(int currentPotentialExpenses, BigDecimal updateAmountValue, boolean isPlus) {
        PotentialExpenses oldPotentialExpenses = potentialExpensesStorage.getPotentialExpensesByPositionInList(currentPotentialExpenses);
        PotentialExpenses newPotentialExpenses = oldPotentialExpenses.clone();
        if (isPlus) {
            newPotentialExpenses.setAmount(oldPotentialExpenses.getAmount().add(updateAmountValue));
        } else {
            newPotentialExpenses.setAmount(oldPotentialExpenses.getAmount().subtract(updateAmountValue));
        }
        potentialExpensesStorage.updatePotentialExpenses(oldPotentialExpenses, newPotentialExpenses);
    }

    public void updatePotentialExpensesBinding(String currentExpensesId, String newPocketNumber, String newCreditCardNumber) {
        PotentialExpenses oldPotentialExpenses = potentialExpensesStorage.getPotentialExpenses(currentExpensesId);
        PotentialExpenses newPotentialExpenses = oldPotentialExpenses.clone();
        newPotentialExpenses.setCardNumber(newCreditCardNumber);
        newPotentialExpenses.setPocketNumber(newPocketNumber);
        potentialExpensesStorage.updatePotentialExpenses(oldPotentialExpenses, newPotentialExpenses);
    }

    public void deletePotentialExpenses(int position) {
        potentialExpensesStorage.deletePotentialExpenses(position);
    }

    public void addCurrentExpenses(CurrentExpenses currentExpenses) {
        currentExpensesStorage.addCurrentExpenses(currentExpenses);
    }

    public ArrayList<CurrentExpenses> getCurrentExpensesList() {
        return currentExpensesStorage.getCurrentExpensesList();
    }

    public void updateCurrentExpensesComment(int currentExpensesPosition, String newComment) {
        CurrentExpenses oldCurrentExpenses = currentExpensesStorage.getCurrentExpensesByPositionInList(currentExpensesPosition);
        CurrentExpenses newCurrentExpenses = oldCurrentExpenses.clone();
        newCurrentExpenses.setComment(newComment);
        currentExpensesStorage.updateCurrentExpenses(oldCurrentExpenses, newCurrentExpenses);
    }

    public void updateCurrentExpensesAmount(int currentExpensesPosition, BigDecimal updateAmountValue, boolean isPlus) {
        CurrentExpenses oldCurrentExpenses = currentExpensesStorage.getCurrentExpensesByPositionInList(currentExpensesPosition);
        CurrentExpenses newCurrentExpenses = oldCurrentExpenses.clone();
        if (isPlus) {
            newCurrentExpenses.setAmount(oldCurrentExpenses.getAmount().add(updateAmountValue));
        } else {
            newCurrentExpenses.setAmount(oldCurrentExpenses.getAmount().subtract(updateAmountValue));
        }
        currentExpensesStorage.updateCurrentExpenses(oldCurrentExpenses, newCurrentExpenses);
    }

    public void updateCurrentExpensesBinding(String currentExpensesId, String newPocketNumber, String newCreditCardNumber) {
        CurrentExpenses oldCurrentExpenses = currentExpensesStorage.getCurrentExpenses(currentExpensesId);
        CurrentExpenses newCurrentExpenses = oldCurrentExpenses.clone();
        newCurrentExpenses.setCardNumber(newCreditCardNumber);
        newCurrentExpenses.setPocketNumber(newPocketNumber);
        currentExpensesStorage.updateCurrentExpenses(oldCurrentExpenses, newCurrentExpenses);
    }

    public void deleteCurrentExpenses(int position) {
        currentExpensesStorage.deleteCurrentExpenses(position);
    }
}
