package files;

import model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertAll;

public class DeleteDataTest extends InfoViewModelTest {

    @Test
    public void deletePocket() {
        viewModel.addPocket(pocket1);
        viewModel.addPocket(pocket2);
        viewModel.addPocket(pocket3);
        viewModel.deletePocket(1);
        ArrayList<Pocket> list = viewModel.getPocketList();
        int size = list.size();
        assertAll(
                () -> Assertions.assertEquals(2, size),
                () -> Assertions.assertEquals(pocket3.getPocketNumber(), list.get(1).getPocketNumber()),
                () -> Assertions.assertEquals(pocket3.getBalance(), list.get(1).getBalance()),
                () -> Assertions.assertEquals(pocket3.getPocketName(), list.get(1).getPocketName())
        );
    }

    @Test
    public void deleteCreditCard() {
        viewModel.addCreditCard(creditCard1);
        viewModel.addCreditCard(creditCard2);
        viewModel.addCreditCard(creditCard3);
        viewModel.deleteCreditCard(1);
        ArrayList<CreditCard> list = viewModel.getCreditCardsList();
        int size = list.size();
        assertAll(
                () -> Assertions.assertEquals(2, size),
                () -> Assertions.assertEquals(creditCard3.getCreditCardNumber(), list.get(1).getCreditCardNumber()),
                () -> Assertions.assertEquals(creditCard3.getBalance(), list.get(1).getBalance()),
                () -> Assertions.assertEquals(creditCard3.getCreditCardName(), list.get(1).getCreditCardName())
        );
    }

    @Test
    public void deleteCurrentExpenses() {
        viewModel.addCurrentExpenses(currentExpenses1);
        viewModel.addCurrentExpenses(currentExpenses2);
        viewModel.addCurrentExpenses(currentExpenses3);
        viewModel.addCurrentExpenses(currentExpenses4);
        viewModel.deleteCurrentExpenses(1);
        viewModel.deleteCurrentExpenses(1);
        ArrayList<CurrentExpenses> list = viewModel.getCurrentExpensesList();
        int size = list.size();
        assertAll(
                () -> Assertions.assertEquals(2, size),
                () -> Assertions.assertEquals(currentExpenses4.getId(), list.get(1).getId()),
                () -> Assertions.assertEquals(currentExpenses4.getAmount(), list.get(1).getAmount()),
                () -> Assertions.assertEquals(currentExpenses4.getComment(), list.get(1).getComment()),
                () -> Assertions.assertEquals(currentExpenses4.getCardNumber(), list.get(1).getCardNumber()),
                () -> Assertions.assertEquals(currentExpenses4.getPocketNumber(), list.get(1).getPocketNumber())
        );
    }

    @Test
    public void deletePotentialExpenses() {
        viewModel.addPotentialExpenses(potentialExpenses1);
        viewModel.addPotentialExpenses(potentialExpenses2);
        viewModel.addPotentialExpenses(potentialExpenses3);
        viewModel.addPotentialExpenses(potentialExpenses4);
        viewModel.deletePotentialExpenses(1);
        viewModel.deletePotentialExpenses(1);
        ArrayList<PotentialExpenses> list = viewModel.getPotentialExpensesList();
        int size = list.size();
        assertAll(
                () -> Assertions.assertEquals(2, size),
                () -> Assertions.assertEquals(potentialExpenses4.getId(), list.get(1).getId()),
                () -> Assertions.assertEquals(potentialExpenses4.getAmount(), list.get(1).getAmount()),
                () -> Assertions.assertEquals(potentialExpenses4.getComment(), list.get(1).getComment()),
                () -> Assertions.assertEquals(potentialExpenses4.getCardNumber(), list.get(1).getCardNumber()),
                () -> Assertions.assertEquals(potentialExpenses4.getPocketNumber(), list.get(1).getPocketNumber())
        );
    }

    @Test
    public void deletePotentialIncome() {
        viewModel.addPotentialIncome(potentialIncome1);
        viewModel.addPotentialIncome(potentialIncome2);
        viewModel.addPotentialIncome(potentialIncome3);
        viewModel.addPotentialIncome(potentialIncome4);
        viewModel.deletePotentialIncome(1);
        viewModel.deletePotentialIncome(1);
        ArrayList<PotentialIncome> list = viewModel.getPPotentialIncomeList();
        int size = list.size();
        assertAll(
                () -> Assertions.assertEquals(2, size),
                () -> Assertions.assertEquals(potentialIncome4.getId(), list.get(1).getId()),
                () -> Assertions.assertEquals(potentialIncome4.getAmount(), list.get(1).getAmount()),
                () -> Assertions.assertEquals(potentialIncome4.getComment(), list.get(1).getComment()),
                () -> Assertions.assertEquals(potentialIncome4.getCardNumber(), list.get(1).getCardNumber()),
                () -> Assertions.assertEquals(potentialIncome4.getPocketNumber(), list.get(1).getPocketNumber())
        );
    }
}
