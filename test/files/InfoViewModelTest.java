package files;

import managment.*;
import model.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import storage.CreditCardStorage;
import storage.CurrentExpensesStorage;
import storage.PotentialExpensesStorage;
import storage.PotentialIncomeStorage;
import viewmodels.InfoViewModel;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class InfoViewModelTest {
    Pocket pocket1 = new Pocket("pocket1", BigDecimal.valueOf(100.01).setScale(2, RoundingMode.HALF_UP));
    Pocket pocket2 = new Pocket("pocket2", BigDecimal.valueOf(200.01).setScale(2, RoundingMode.HALF_UP));
    Pocket pocket3 = new Pocket("pocket3", BigDecimal.valueOf(200.01).setScale(2, RoundingMode.HALF_UP));
    CreditCard creditCard1 = new CreditCard("creditCard1", BigDecimal.valueOf(100.01).setScale(2, RoundingMode.HALF_UP));
    CreditCard creditCard2 = new CreditCard("creditCard2", BigDecimal.valueOf(200.01).setScale(2, RoundingMode.HALF_UP));
    CreditCard creditCard3 = new CreditCard("creditCard3", BigDecimal.valueOf(300.01).setScale(2, RoundingMode.HALF_UP));
    CurrentExpenses currentExpenses1 = new CurrentExpenses(BigDecimal.valueOf(100.01).setScale(2, RoundingMode.HALF_UP), pocket1.getPocketNumber(), "", "списание с testPocket1");
    CurrentExpenses currentExpenses2 = new CurrentExpenses(BigDecimal.valueOf(200.01).setScale(2, RoundingMode.HALF_UP), pocket2.getPocketNumber(), "", "списание с testPocket2");
    CurrentExpenses currentExpenses3 = new CurrentExpenses(BigDecimal.valueOf(300.01).setScale(2, RoundingMode.HALF_UP), "", creditCard1.getCreditCardNumber(), "списание с creditCard1");
    CurrentExpenses currentExpenses4 = new CurrentExpenses(BigDecimal.valueOf(400.01).setScale(2, RoundingMode.HALF_UP), "", creditCard2.getCreditCardNumber(), "списание с creditCard2");
    PotentialExpenses potentialExpenses1 = new PotentialExpenses(BigDecimal.valueOf(100.01).setScale(2, RoundingMode.HALF_UP), pocket1.getPocketNumber(), "", "списание с testPocket1");
    PotentialExpenses potentialExpenses2 = new PotentialExpenses(BigDecimal.valueOf(200.01).setScale(2, RoundingMode.HALF_UP), pocket2.getPocketNumber(), "", "списание с testPocket2");
    PotentialExpenses potentialExpenses3 = new PotentialExpenses(BigDecimal.valueOf(300.01).setScale(2, RoundingMode.HALF_UP), "", creditCard1.getCreditCardNumber(), "списание с creditCard1");
    PotentialExpenses potentialExpenses4 = new PotentialExpenses(BigDecimal.valueOf(400.01).setScale(2, RoundingMode.HALF_UP), "", creditCard2.getCreditCardNumber(), "списание с creditCard2");
    PotentialIncome potentialIncome1 = new PotentialIncome(BigDecimal.valueOf(100.01).setScale(2, RoundingMode.HALF_UP), pocket1.getPocketNumber(), "", "списание с testPocket1");
    PotentialIncome potentialIncome2 = new PotentialIncome(BigDecimal.valueOf(200.01).setScale(2, RoundingMode.HALF_UP), pocket2.getPocketNumber(), "", "списание с testPocket2");
    PotentialIncome potentialIncome3 = new PotentialIncome(BigDecimal.valueOf(300.01).setScale(2, RoundingMode.HALF_UP), "", creditCard1.getCreditCardNumber(), "списание с creditCard1");
    PotentialIncome potentialIncome4 = new PotentialIncome(BigDecimal.valueOf(400.01).setScale(2, RoundingMode.HALF_UP), "", creditCard2.getCreditCardNumber(), "списание с creditCard2");
    static final FileManager fileManager = new FileManager();
    static PocketStorageImpl pocketStorage;
    static CreditCardStorage creditCardStorage;
    static CurrentExpensesStorage currentExpensesStorage;
    static PotentialExpensesStorage potentialExpensesStorage;
    static PotentialIncomeStorage potentialIncomeStorage;
    static private final String dirName = "database";
    static InfoViewModel viewModel;

    @BeforeAll
    static void setUp() {
        pocketStorage = new PocketStorageImpl(fileManager);
        creditCardStorage = new CreditCardStorageImpl(fileManager);
        currentExpensesStorage = new CurrentExpensesStorageImpl(fileManager);
        potentialExpensesStorage = new PotentialExpensesStorageImpl(fileManager);
        potentialIncomeStorage = new PotentialIncomeStorageImpl(fileManager);
        viewModel = new InfoViewModel(pocketStorage, creditCardStorage, currentExpensesStorage, potentialExpensesStorage, potentialIncomeStorage);
    }

    @BeforeEach
    void beforeEach() {
        viewModel.init();
    }

    @AfterEach
    void afterEach() {
        fileManager.deleteDirectory(dirName);
    }

//    @org.junit.jupiter.api.Test
//    void addPocket() {
//        pocketStorage.addPocket(pocket1);
//        pocketStorage.addPocket(pocket2);
//        int size = pocketStorage.getPocketList().size();
//        Assertions.assertEquals(2, size);
//    }


}