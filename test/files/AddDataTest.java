package files;

import org.junit.jupiter.api.*;

public class AddDataTest extends InfoViewModelTest {

    @Test()
    @DisplayName("Добавление кошелька")
    public void addPocket() {
        viewModel.addPocket(pocket1);
        viewModel.addPocket(pocket2);
        viewModel.addPocket(pocket3);
        int size = viewModel.getPocketList().size();
        Assertions.assertEquals(3, size);
    }

    @Test
    @DisplayName("Добавление кредитной карты")
    public void addCreditCard() {
        viewModel.addCreditCard(creditCard1);
        viewModel.addCreditCard(creditCard2);
        viewModel.addCreditCard(creditCard3);
        int size = viewModel.getCreditCardsList().size();
        Assertions.assertEquals(3, size);
    }

    @Test
    @DisplayName("Добавление текущих расходов")
    public void addCurrentExpenses() {
        viewModel.addCurrentExpenses(currentExpenses1);
        viewModel.addCurrentExpenses(currentExpenses2);
        viewModel.addCurrentExpenses(currentExpenses3);
        viewModel.addCurrentExpenses(currentExpenses4);
        int size = viewModel.getCurrentExpensesList().size();
        Assertions.assertEquals(4, size);
    }

    @Test
    @DisplayName("Добавление потенциальных расходов")
    public void addPotentialExpenses() {
        viewModel.addPotentialExpenses(potentialExpenses1);
        viewModel.addPotentialExpenses(potentialExpenses2);
        viewModel.addPotentialExpenses(potentialExpenses3);
        viewModel.addPotentialExpenses(potentialExpenses4);
        int size = viewModel.getPotentialExpensesList().size();
        Assertions.assertEquals(4, size);
    }

    @Test
    @DisplayName("Добавление потенциальных доходов")
    public void addPotentialIncome() {
        viewModel.addPotentialIncome(potentialIncome1);
        viewModel.addPotentialIncome(potentialIncome2);
        viewModel.addPotentialIncome(potentialIncome3);
        viewModel.addPotentialIncome(potentialIncome4);
        int size = viewModel.getPPotentialIncomeList().size();
        Assertions.assertEquals(4, size);
    }


}
